/*
    Blink

    Turns an LED on for one second, then off for one second, repeatedly.

    Most Arduinos have an on-board LED you can control. On the UNO, MEGA and ZERO
    it is attached to digital pin 13, on MKR1000 on pin 6. LED_BUILTIN is set to
    the correct LED pin independent of which board is used.
    If you want to know what pin the on-board LED is connected to on your Arduino
    model, check the Technical Specs of your board at:
    https://www.arduino.cc/en/Main/Products

    modified 8 May 2014
    by Scott Fitzgerald
    modified 2 Sep 2016
    by Arturo Guadalupi
    modified 8 Sep 2016
    by Colby Newman

    This example code is in the public domain.

    http://www.arduino.cc/en/Tutorial/Blink
*/

// the setup function runs once when you press reset or power the board
void setup() {
    // initialize digital pin LED_BUILTIN as an output.
    pinMode(LED_BUILTIN, OUTPUT);
    digitalWrite(LED_BUILTIN, HIGH);
    Serial.begin(115200);
}

// the loop function runs over and over again forever
void loop() {
    String s("Arduino");
    
    if (Serial.available())
    {
        String input = Serial.readString();
        if (s == input)
        {
            Serial.println("Hello Arduino !");
        }
        else
        {
            Serial.println(String("Hello " + input));
        }
    }
    
    static unsigned long prev;
//  if (millis() > prev + 3000)  //在50天左右，此判断会出错
    if (millis() - prev > 3000)
    {
        static int state = HIGH;
        if (HIGH == state)
            state = LOW;
        else
            state = HIGH;
        digitalWrite(LED_BUILTIN, state);
        prev = millis();
    }
}
