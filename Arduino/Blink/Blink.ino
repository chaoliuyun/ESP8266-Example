//LED点灯

void setup() {
  pinMode(LED_BUILTIN, OUTPUT);     //将LED_BUILTIN引脚设置为输出模式
}

void loop() {
  digitalWrite(LED_BUILTIN, LOW);   //将引脚设置为低电平，点亮LED
  delay(1000);                      //延时1秒
  digitalWrite(LED_BUILTIN, HIGH);  //关闭LED
  delay(1000);
}
