#include "stepper_28BYJ48.h"

Stepper_28BYJ48::Stepper_28BYJ48(int IN1, int IN2, int IN3, int IN4)
{
    _IN1 = IN1;
    _IN2 = IN2;
    _IN3 = IN3;
    _IN4 = IN4;

    pinMode(_IN1, OUTPUT);
    pinMode(_IN2, OUTPUT);
    pinMode(_IN3, OUTPUT);
    pinMode(_IN4, OUTPUT);
};

void Stepper_28BYJ48::step( int count) {
    while ( count > 0 ) {
        for (int i = 0; i < 8; i++)
        {
            setOutput(i);
            delay(1);
        }
        count--;
    }

    while ( count < 0 ) {
        for (int i = 7; i >= 0; i--)
        {
            setOutput(i);
            delay(1);
        }
        count++;
    }
};

void Stepper_28BYJ48::setOutput(int out) {
    digitalWrite(_IN1, bitRead(lookup[out], 0));
    digitalWrite(_IN2, bitRead(lookup[out], 1));
    digitalWrite(_IN3, bitRead(lookup[out], 2));
    digitalWrite(_IN4, bitRead(lookup[out], 3));
};
